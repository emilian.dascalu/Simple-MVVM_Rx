package com.example.emilian.mvvm;

import android.support.annotation.NonNull;

/**
 * Created by Emilian on 12/7/2017.
 */

public class MyApplication extends android.app.Application {
    @NonNull
    private final DataModelListener dataModelListener;

    public MyApplication() {
        dataModelListener = new DataModel();
    }

    @NonNull
    public DataModelListener getDataModel() {
        return dataModelListener;
    }

    @NonNull
    public SchedulerProviderListener getSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }

    @NonNull
    public MainViewModel getViewModel() {
        return new MainViewModel(getDataModel(), getSchedulerProvider());
    }
}
