package com.example.emilian.mvvm;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Emilian on 12/7/2017.
 */

public interface DataModelListener {

    @NonNull
    Observable<String> getData(String s);

    @NonNull
    Observable<List<String>> getStringListData(List<String> stringList);
}
