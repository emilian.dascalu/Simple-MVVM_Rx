package com.example.emilian.mvvm;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Emilian on 12/7/2017.
 */

public class MainViewModel {

    private DataModelListener dataModelListener;

    private final BehaviorSubject<String> stringData = BehaviorSubject.create();
    private final BehaviorSubject<List<String>> listStringData = BehaviorSubject.create();
    @NonNull
    private final SchedulerProviderListener schedulerProviderListener;

    public MainViewModel(DataModelListener dataModelListener, @NonNull SchedulerProviderListener schedulerProviderListener) {
        this.dataModelListener = dataModelListener;
        this.schedulerProviderListener = schedulerProviderListener;
    }

    public Observable<String> getStringData() {
        return stringData.observeOn(schedulerProviderListener.computation())
                .flatMap(dataModelListener :: getData);
    }

    public Observable<List<String>> getStringListData() {
        return listStringData.observeOn(schedulerProviderListener.computation())
                .flatMap(dataModelListener :: getStringListData);
    }

    public void buttonClickEvent() {
        stringData.onNext("");
        listStringData.onNext(new ArrayList<>());
    }
}
