package com.example.emilian.mvvm;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Emilian on 12/7/2017.
 */

class SchedulerProvider implements SchedulerProviderListener{
    private static SchedulerProvider ourInstance;

    private SchedulerProvider() {
    }

    public static SchedulerProvider getInstance() {
        if (ourInstance == null) {
            ourInstance = new SchedulerProvider();
        }
        return ourInstance;
    }

    @Override
    @NonNull
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    @NonNull
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
