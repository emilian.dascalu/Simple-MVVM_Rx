package com.example.emilian.mvvm;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private CompositeDisposable compositeDisposable;
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button mvvmButton = findViewById(R.id.mvvm_button);
        mvvmButton.setOnClickListener(this);
        mainViewModel = ((MyApplication) getApplication()).getViewModel();

        //runs a simple observable - observer environment
        runSimpleRx();
    }

    private void runSimpleRx() {
        Observer<List<String>> observer = new Observer<List<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<String> strings) {
                for(String s : strings) {
                    Log.e("Observable", "Received new String: " + s);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        mainViewModel.getStringListData().subscribe(observer);
    }

    private void bind() {
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(mainViewModel.getStringData()
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(this :: showData));
    }

    private void showData(@NonNull final String data) {
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
    }

    private void unBind() {
        compositeDisposable.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bind();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unBind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mvvm_button:
                mainViewModel.buttonClickEvent();
                break;
        }
    }
}
