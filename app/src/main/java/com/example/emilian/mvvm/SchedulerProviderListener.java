package com.example.emilian.mvvm;

import io.reactivex.Scheduler;

/**
 * Created by Emilian on 12/7/2017.
 */

public interface SchedulerProviderListener {

    Scheduler computation();
    Scheduler ui();
}
