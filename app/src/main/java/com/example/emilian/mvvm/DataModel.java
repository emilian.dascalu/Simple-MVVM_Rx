package com.example.emilian.mvvm;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Emilian on 12/6/2017.
 */

public class DataModel implements DataModelListener{

    private String simpleString = "Testing MVVM";

    public String getSimpleString() {
        return simpleString;
    }

    @NonNull
    @Override
    public Observable<String> getData(String s) {
        return Observable.just(getSimpleString());
    }

    @NonNull
    @Override
    public Observable<List<String>> getStringListData(List<String> stringList) {
        return getStringListData();
    }

    private Observable<List<String>> getStringListData () {
        List<String> sampleStrings = new ArrayList<>();
        sampleStrings.add("firsst");
        sampleStrings.add("second");
        sampleStrings.add("third");
        return Observable.just(sampleStrings);
    }
}
